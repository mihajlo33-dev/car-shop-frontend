import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SharedService {
readonly APIUrl = "http://127.0.0.1:8000";
readonly PhotoUrl = "http://127.0.0.1:8000/media/";

  constructor(private http:HttpClient) { }


  getBrandList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/brand/');
  }

  addBrand(val:any){
    return this.http.post(this.APIUrl + '/brand/',val);
  }

  updateBrand(val:any){
    return this.http.put(this.APIUrl + '/brand/',val);
  }

  deleteBrand(val:any){
    return this.http.delete(this.APIUrl + '/brand/' + val);
  }



  getModelCarList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/modelCar/');
  }

  addModelCar(val:any){
    return this.http.post(this.APIUrl + '/modelCar/',val);
  }

  updateModelCar(val:any){
    return this.http.put(this.APIUrl + '/modelCar/',val);
  }

  deleteModelCar(val:any){
    return this.http.delete(this.APIUrl + '/modelCar/' + val);
  }


  getPriceList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/price/');
  }

  addPrice(val:any){
    return this.http.post(this.APIUrl + '/price/',val);
  }

  updatePrice(val:any){
    return this.http.put(this.APIUrl + '/price/',val);
  }

  deletePrice(val:any){
    return this.http.delete(this.APIUrl + '/price/' + val);
  }



  getYearList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/year/');
  }

  addYear(val:any){
    return this.http.post(this.APIUrl + '/year/',val);
  }

  updateYear(val:any){
    return this.http.put(this.APIUrl + '/year/',val);
  }

  deleteYear(val:any){
    return this.http.delete(this.APIUrl + '/year/' + val);
  }

 getFuelList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/fuel/');
  }

  addFuel(val:any){
    return this.http.post(this.APIUrl + '/fuel/',val);
  }

  updateFuel(val:any){
    return this.http.put(this.APIUrl + '/fuel/',val);
  }

  deleteFuel(val:any){
    return this.http.delete(this.APIUrl + '/fuel/' + val);
  }


  getCarList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/car/');
  }

  addCar(val:any){
    return this.http.post(this.APIUrl + '/car/',val);
  }

  updateCar(val:any){
    return this.http.put(this.APIUrl + '/car/',val);
  }

  deleteCar(val:any){
    return this.http.delete(this.APIUrl + '/car/' + val);
  }


  UploadPhoto(val:any){
    return this.http.post(this.APIUrl + '/SaveFile/',val);
  }

  getAllCarNames():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/car/');
  }


  getPhotos():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/media/');
  }

  getCarById(id:number):Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/car/:id');
  }


}
