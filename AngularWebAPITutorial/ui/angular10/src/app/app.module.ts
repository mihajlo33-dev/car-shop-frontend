import { NgModule,/*NO_ERRORS_SCHEMA*/ } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrandComponent } from './brand/brand.component';
import { ModelCarComponent } from './model-car/model-car.component';
import { PriceComponent } from './price/price.component';
import { YearComponent } from './year/year.component';
import { FuelComponent } from './fuel/fuel.component';
import { CarComponent } from './car/car.component';
import { ShowCarComponent } from './car/show/show.component';
import { AddEditCarComponent } from './car/add-edit/add-edit.component';
import { ShowComponent } from './brand/show/show.component';
import { AddEditComponent } from './brand/add-edit/add-edit.component';
import{SharedService} from './shared.service';
import{HttpClientModule} from '@angular/common/http';
import{FormsModule,ReactiveFormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { CarDetailsComponent } from './car/car-details/car-details.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';

@NgModule({
  declarations: [
    AppComponent,
    BrandComponent,
    ModelCarComponent,
    PriceComponent,
    YearComponent,
    FuelComponent,
    CarComponent,
    ShowComponent,
    AddEditComponent,
    ShowCarComponent,
    AddEditCarComponent,
    CarDetailsComponent,
    AboutComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    RouterTestingModule
  ],
  providers: [SharedService],
  bootstrap: [AppComponent],
  //schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule { }
