import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { from } from 'rxjs';
import {BrandComponent} from './brand/brand.component';
import {ModelCarComponent} from './model-car/model-car.component';
import {PriceComponent} from './price/price.component';
import {YearComponent} from './year/year.component';
import {FuelComponent} from './fuel/fuel.component';
import {CarComponent} from './car/car.component';
import {ContactComponent} from './contact/contact.component';
import {AboutComponent} from './about/about.component';
import { CarDetailsComponent } from './car/car-details/car-details.component';



const routes: Routes = [
  {path:'brand',component:BrandComponent},
  {path:'modelCar',component:ModelCarComponent},
  {path:'price',component:PriceComponent},
  {path:'year',component:YearComponent},
  {path:'fuel',component:FuelComponent},
  {path:'car',component:CarComponent},
  {path:'contact',component:ContactComponent},
  {path:'about',component:AboutComponent},
  {path:':id',component:CarDetailsComponent}


  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
