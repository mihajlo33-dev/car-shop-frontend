import { Component, OnInit,Input } from '@angular/core';
import { BrandComponent } from 'src/app/brand/brand.component';
import{SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-car',
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.css']
})
export class AddEditCarComponent implements OnInit,BrandComponent {

  constructor(private service:SharedService) { }

  @Input() car:any;
  carId?:string ;
  carBrand?: string;
  carModel?:string;
  carYear?:string;
  carPrice?:string;
  carFuel?:string;
  carDescription?:string;
  DateOfJoining?:string;
  PhotoFileName?:string;
  PhotoFilePath?:string;

  CarList:any=[];

  ngOnInit(): void {
    this.loadCarList();
  }

  loadCarList(){
    this.service.getAllCarNames().subscribe((data:any)=>{
      this.CarList=data;

      this.carId=this.car.carId;
      this.carBrand=this.car.carBrand;
      this.carModel = this.car.carModel;
      this.carYear = this.car.carYear;
      this.carPrice = this.car.carPrice;
      this.carFuel = this.car.carFuel;
      this.carDescription = this.car.carDescription;
      this.DateOfJoining = this.car.DateOfJoining;
      this.PhotoFileName = this.car.PhotoFileName;
      this.PhotoFilePath = this.service.PhotoUrl+this.PhotoFileName;

    });
  }

  addCar(){
    var val = {carId:this.carId,carBrand:this.carBrand,carModel:this.carModel,carYear:this.carYear,carPrice:this.carPrice,carFuel:this.carFuel,carDescription:this.carDescription,DateOfJoining:this.DateOfJoining,PhotoFileName:this.PhotoFileName,PhotoFilePath:this.PhotoFilePath};
    this.service.addCar(val).subscribe(res=>{alert(res.toString());
    });
  }

  updateCar(){
    var val = {carId:this.carId,carBrand:this.carBrand,carModel:this.carModel,carYear:this.carYear,carPrice:this.carPrice,carFuel:this.carFuel,carDescription:this.carDescription,DateOfJoining:this.DateOfJoining,PhotoFileName:this.PhotoFileName,PhotoFilePath:this.PhotoFilePath};
    this.service.updateCar(val).subscribe(res=>{alert(res.toString());
    });
  }

  uploadPhoto(event:any){
    var file = event.target.files[0];
    const formData:FormData = new FormData();
    formData.append('uploadedFile',file,file.name);

    this.service.UploadPhoto(formData).subscribe((data:any)=>{
      this.PhotoFileName = data.toString();
      this.PhotoFilePath = this.service.PhotoUrl+this.PhotoFileName;
    })
  }
}
