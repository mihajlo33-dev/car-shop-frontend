import { Component, OnInit } from '@angular/core';
import{SharedService} from 'src/app/shared.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-show-car',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowCarComponent implements OnInit {

  constructor(private service:SharedService,private route: ActivatedRoute,private router:Router) { }

  CarList:any = [];


  ModalTitle?:string;
  ActivateAddEditCarComp:boolean=false;
  car:any;
  

  carIdFilter:string="";
  carBrandFilter:string="";
  carModelFilter:string="";
  carYearFilter:string="";
  carPriceFilter:string="";
  carFuelFilter:string="";
  CarListWithoutFilter:any = [];
  PhotoFilePath?:string;
  PhotoFileName?:string;
  private sub: any;
  idParam: string = "";
  selectedCar?:string;

  ngOnInit(): void {
    this.refreshCarList();
    this.sub = this.route.params.subscribe(params => {
      this.idParam = params['id'];
    });
      //make you api call here
  }

  onSelect(item:any){
    this.selectedCar = item;
    this.router.navigateByUrl("/car-details/" + this.car.carId);

  }

  addClick(){
    this.car={
      carId:0,
      carName:"",
      DateOfJoining:"",
      PhotoFileName:"bmw.jpg",
    }
    this.ModalTitle="Add Car";
    this.ActivateAddEditCarComp=true;
  }

  editClick(item:any){
    this.car = item;
    this.ModalTitle = "Edit Car";
    this.ActivateAddEditCarComp = true;
  }

  closeClick(){
    this.ActivateAddEditCarComp = false;
    this.refreshCarList();
  }



  deleteClick(item:any){
    if(confirm('Are you sure??')){
      this.service.deleteCar(item.carId).subscribe(data=>{alert(data.toString());
      this.refreshCarList();
      })
    }
  }

  refreshCarList(){
    this.service.getCarList().subscribe(
      data=>{
        this.CarList = data;
        this.CarListWithoutFilter = data;
      });
  }

  FilterFn(){
    var carIdFilter = this.carIdFilter;
    var carBrandFilter = this.carBrandFilter;
    var carModelFilter = this.carModelFilter;
    var carYearFilter = this.carYearFilter;
    var carPriceFilter = this.carPriceFilter;
    var carFuelFilter = this.carFuelFilter;

    this.CarList = this.CarListWithoutFilter(function(el:any){
      return el.carBrand.toString().toLowerCase().includes(
          carBrandFilter.toString().trim().toLowerCase()
        )
        &&
        el.carModel.toString().toLowerCase().includes(
          carModelFilter.toString().trim().toLowerCase()
        )
        &&
        el.carYear.toString().toLowerCase().includes(
          carYearFilter.toString().trim().toLowerCase()
        )
        &&
        el.carPrice.toString().toLowerCase().includes(
          carPriceFilter.toString().trim().toLowerCase()
        )
        &&
        el.carFuel.toString().toLowerCase().includes(
          carFuelFilter.toString().trim().toLowerCase()
        )
    });
  }

  uploadPhoto(event:any){
    var file = event.target.files[0];
    const formData:FormData = new FormData();
    formData.append('uploadedFile',file,file.name);

    this.service.UploadPhoto(formData).subscribe((data:any)=>{
      this.PhotoFileName = data.toString();
      this.PhotoFilePath = this.service.PhotoUrl+this.PhotoFileName;
    })
  }

  

}
