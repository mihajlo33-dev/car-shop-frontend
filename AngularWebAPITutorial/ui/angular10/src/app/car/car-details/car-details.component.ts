import { Component, Input, OnInit } from '@angular/core';
import{SharedService} from 'src/app/shared.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-car-details',
  templateUrl: './car-details.component.html',
  styleUrls: ['./car-details.component.css']
})
export class CarDetailsComponent implements OnInit {

  @Input() car:any;

  constructor(private service:SharedService,private route: ActivatedRoute,private router:Router) { }

  ngOnInit(): void {
    let id = + this.route.snapshot.params['id'];
    this.service.getCarById(id).subscribe(result => this.car = result);
  }

}
